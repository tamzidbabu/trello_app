package com.example.tamzid.trelloapp.activity;

import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.FragmentManager;
import android.support.v4.view.ViewPager;
import android.support.v7.app.AlertDialog;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;

import com.example.tamzid.trelloapp.R;
import com.example.tamzid.trelloapp.adapter.ListFragmentPagerAdapter;
import com.example.tamzid.trelloapp.model.Board;
import com.example.tamzid.trelloapp.model.Lists;
import com.example.tamzid.trelloapp.model.NewBoard;
import com.example.tamzid.trelloapp.rest.ApiClient;
import com.example.tamzid.trelloapp.rest.ApiInterface;

import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

/**
 * Created by TAMZID on 1/2/2017.
 */

public class AddListFragment extends android.support.v4.app.Fragment {

    Button addListButton;
    Board board;
    ApiInterface apiService =
            ApiClient.getClient().create(ApiInterface.class);
    String boardId;


    Intent intent;
    List<Lists> lists;


    ListFragmentPagerAdapter listFragmentPagerAdapter;
    public AddListFragment() {

    }



    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View item_view = inflater.inflate(R.layout.add_list_fragment, container, false);
        addListButton = (Button) item_view.findViewById(R.id.btn_add_list);

        ListActivity activity = (ListActivity) getActivity();
        boardId = activity.getMyData();

        addListButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                AddNewList(boardId);

            }
        });


        return item_view;
    }

    private void AddNewList(final String boardId) {

        // get new_board_prompt.xml view
        LayoutInflater li = LayoutInflater.from(getContext());
        View promptsView = li.inflate(R.layout.new_board_prompt, null);

        AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(
                getContext());

        // set new_board_prompt.xml to alertdialog builder
        alertDialogBuilder.setView(promptsView);

        final EditText userInput = (EditText) promptsView
                .findViewById(R.id.prompt_edit_text);
        final TextView tv = (TextView) promptsView.findViewById(R.id.prompt_text_view);
        tv.setText("Add New List: ");

        // set dialog message
        alertDialogBuilder
                .setCancelable(false)
                .setPositiveButton("OK",
                        new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog, int id) {

                                Call<NewBoard> call = apiService.addList(userInput.getText().toString(), boardId);
                                call.enqueue(new Callback<NewBoard>() {
                                    @Override
                                    public void onResponse(Call<NewBoard> call, Response<NewBoard> response) {

                                        try {

                                            AddLists();

                                            intent = new Intent(getContext(), com.example.tamzid.trelloapp.activity.ListActivity.class);

                                            intent.putExtra("id", boardId);
                                            startActivity(intent);
                                        } catch (Exception e) {
                                            Log.d("onResponse", "There is an error");
                                            e.printStackTrace();
                                        }
                                    }

                                    @Override
                                    public void onFailure(Call<NewBoard> call, Throwable t) {

                                    }
                                });
//


                            }
                        })
                .setNegativeButton("Cancel",
                        new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog, int id) {
                                dialog.cancel();
                            }
                        });

        // create alert dialog
        AlertDialog alertDialog = alertDialogBuilder.create();

        // show it
        alertDialog.show();


    }

    private void AddLists() {

        final ApiInterface apiService =
                ApiClient.getClient().create(ApiInterface.class);
        Call<List<Lists>> call = apiService.getList(boardId);
        call.enqueue(new Callback<List<Lists>>() {
            @Override
            public void onResponse(Call<List<Lists>> call, Response<List<Lists>> response) {
                lists = response.body();




            }

            @Override
            public void onFailure(Call<List<Lists>> call, Throwable t) {

            }
        });

    }

}
