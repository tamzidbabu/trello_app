package com.example.tamzid.trelloapp.activity;

import android.content.Intent;
import android.support.v4.app.NavUtils;
import android.support.v4.view.ViewPager;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.MenuItem;

import com.example.tamzid.trelloapp.R;
import com.example.tamzid.trelloapp.adapter.ListFragmentPagerAdapter;
import com.example.tamzid.trelloapp.model.Cards;
import com.example.tamzid.trelloapp.model.Lists;
import com.example.tamzid.trelloapp.rest.ApiClient;
import com.example.tamzid.trelloapp.rest.ApiInterface;

import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class ListActivity extends AppCompatActivity {
    String boardId, boardName;
    ViewPager viewPager;
    List<Lists> lists;


    ListFragmentPagerAdapter listFragmentPagerAdapter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_list);
        boardName = getIntent().getStringExtra("name");
        boardId = getIntent().getStringExtra("id");


        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setTitle(boardName);

        viewPager = (ViewPager) findViewById(R.id.list_view_pager);
        // Disable clip to padding
        viewPager.setClipToPadding(false);
        // set padding manually, the more you set the padding the more you see of prev & next page
        viewPager.setPadding(10, 0, 50, 0);
        // sets a margin b/w individual pages to ensure that there is a gap b/w them, increasing the number
        // increse the gap
        viewPager.setPageMargin(20);

        AddLists();


    }


    private void AddLists() {

        final ApiInterface apiService =
                ApiClient.getClient().create(ApiInterface.class);
        Call<List<Lists>> call = apiService.getList(boardId);
        call.enqueue(new Callback<List<Lists>>() {
            @Override
            public void onResponse(Call<List<Lists>> call, Response<List<Lists>> response) {
                lists = response.body();

                listFragmentPagerAdapter = new ListFragmentPagerAdapter(getSupportFragmentManager(), lists);
                viewPager.setAdapter(listFragmentPagerAdapter);


            }

            @Override
            public void onFailure(Call<List<Lists>> call, Throwable t) {

            }
        });

    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            // Respond to the action bar's Up/Home button
            case android.R.id.home:
                NavUtils.navigateUpFromSameTask(this);
                return true;
        }
        return super.onOptionsItemSelected(item);
    }

    @Override
    public void onBackPressed() {
        startActivity(new Intent(this, MainActivity.class));
    }

    public String getMyData() {
        return boardId;
    }
}
