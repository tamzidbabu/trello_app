package com.example.tamzid.trelloapp.activity;

import android.app.*;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.AlertDialog;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.example.tamzid.trelloapp.R;
import com.example.tamzid.trelloapp.adapter.CardsAdapter;
import com.example.tamzid.trelloapp.model.AddCard;
import com.example.tamzid.trelloapp.model.Board;
import com.example.tamzid.trelloapp.model.Cards;
import com.example.tamzid.trelloapp.model.Lists;
import com.example.tamzid.trelloapp.model.NewBoard;
import com.example.tamzid.trelloapp.rest.ApiClient;
import com.example.tamzid.trelloapp.rest.ApiInterface;
import com.example.tamzid.trelloapp.utill.RecyclerTouchListener;

import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

/**
 * Created by TAMZID on 12/27/2016.
 */

public class ListFragments extends android.support.v4.app.Fragment {
    Lists listsItem;
    List<Cards> cards;
    private String TAG = "PagerFragment";
    private String title, cardsTitle, cardsDes, cardsId;
    TextView listName;
    Button button_add_card;
    Context context;
    CardsAdapter cardsAdapter;
    RecyclerView recyclerView;
    Intent intent;
    ApiInterface apiService =
            ApiClient.getClient().create(ApiInterface.class);


    public ListFragments() {
    }

    public static ListFragments getInstance(/*String imageUrl, String title*/) {
        ListFragments listFragments = new ListFragments();
        //We will not pass the data through setArguments() method using bundle because it will not gets updated by calling notifyDataSetChanged()  method. We will do it through getter and setter.
        //Bundle bundle = new Bundle();
        //bundle.putString(Utils.EXTRA_IMAGE_URL, imageUrl);
        //bundle.putString(Utils.EXTRA_TITLE, title);
        //fragment.setArguments(bundle);
        return listFragments;
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, final ViewGroup container, Bundle savedInstanceState) {
        View item_view = inflater.inflate(R.layout.list_item_cardview, container, false);

        listName = (TextView) item_view.findViewById(R.id.list_name);
        recyclerView = (RecyclerView) item_view.findViewById(R.id.card_recyclerView);
        button_add_card = (Button) item_view.findViewById(R.id.button_add_card);

        title = listsItem.getName();


        listName.setText(title);
//
//        button_add_card.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View v) {
//                Toast.makeText(context, "Add cards", Toast.LENGTH_SHORT).show();
//               // AddNewCard();
//            }
//        });

        button_add_card.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Log.i("button click", listsItem.getId());
                AddNewCard();
            }
        });


        recyclerView.setLayoutManager(new LinearLayoutManager(context));
        recyclerView.setItemAnimator(new DefaultItemAnimator());
        recyclerView.setHasFixedSize(true);

        CardsCall();

        recyclerViewItemClick();


        return item_view;
    }

    private void CardsCall() {

        Call<List<Cards>> newCall = apiService.getCards(listsItem.getId());
        newCall.enqueue(new Callback<List<Cards>>() {
            @Override
            public void onResponse(Call<List<Cards>> call, Response<List<Cards>> response) {

                cards = response.body();
                for (int i = 0; i < cards.size(); i++) {
                    Log.i("cards response", cards.get(i).getId());
                }

                Log.i("list id", listsItem.getId());
                cardsAdapter = new CardsAdapter(cards, R.layout.list_item_cardview_items, context);
                recyclerView.setAdapter(cardsAdapter);


            }

            @Override
            public void onFailure(Call<List<Cards>> call, Throwable t) {

            }
        });

    }

    private void AddNewCard() {

        // get new_board_prompt.xml view
        LayoutInflater li = LayoutInflater.from(getContext());
        View promptsView = li.inflate(R.layout.add_card_prompt, null);

        AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(
                getContext());

        // set new_board_prompt.xml to alertdialog builder
        alertDialogBuilder.setView(promptsView);

        final EditText title = (EditText) promptsView
                .findViewById(R.id.edit_text_card_title);
        final EditText description = (EditText) promptsView.findViewById(R.id.edit_text_card_des);

        // set dialog message
        alertDialogBuilder
                .setCancelable(false)
                .setPositiveButton("ADD CARD",
                        new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog, int id) {

                                Call<AddCard> call = apiService.postNewCard(title.getText().toString(), description.getText().toString(), listsItem.getId());
                                call.enqueue(new Callback<AddCard>() {
                                    @Override
                                    public void onResponse(Call<AddCard> call, Response<AddCard> response) {

                                        try {
                                            //newBoards = response.body();
                                            cardsTitle = response.body().getName();
                                            cardsDes = response.body().getDesc();
                                            cardsId = response.body().getId();
                                            CardsCall();
                                            //    cardsAdapter.notifyDataSetChanged();

                                            intent = new Intent(getContext(), ListFragments.class);
////                                            intent.putExtra("name",nameNewBoard);
////                                            intent.putExtra("id", idNewBoard);
                                            startActivity(intent);

                                        } catch (Exception e) {
                                            Log.d("onResponse", "There is an error");
                                            e.printStackTrace();
                                        }
                                    }

                                    @Override
                                    public void onFailure(Call<AddCard> call, Throwable t) {

                                    }
                                });
                            }
                        })
                .setNegativeButton("Cancel",
                        new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog, int id) {
                                dialog.cancel();
                            }
                        });

        // create alert dialog
        AlertDialog alertDialog = alertDialogBuilder.create();

        // show it
        alertDialog.show();
    }


    public Lists getListsItem() {

        return listsItem;
    }


    public void setListsItem(Lists listsItem) {
        this.listsItem = listsItem;

        //  Log.i(TAG,"setListsItem:ID:"+listsItem.getId());
    }


    //working with recycler view single item,,,
    private void recyclerViewItemClick() {
        recyclerView.addOnItemTouchListener(new RecyclerTouchListener(getContext(), recyclerView, new RecyclerTouchListener.ClickListener() {
            @Override
            public void onClick(View view, int position) {


            }

            @Override
            public void onLongClick(View view, final int position) {

                LayoutInflater li = LayoutInflater.from(getContext());
                View promptsView = li.inflate(R.layout.card_longclick_prompt, null);

                AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(
                        getContext());

                final Button btnCardRename = (Button) promptsView.findViewById(R.id.button_card_rename);
                final Button btnCardRemove = (Button) promptsView.findViewById(R.id.button_card_remove);


                // set new_board_prompt.xml to alertdialog builder
                alertDialogBuilder.setView(promptsView);
                alertDialogBuilder
                        .setCancelable(true)
                        .setTitle(cards.get(position).getName());
                // create alert dialog
                final AlertDialog alertDialog1 = alertDialogBuilder.create();
                // show it
                alertDialog1.show();

                btnCardRename.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        alertDialog1.hide();
                        renameCard(position);


                    }
                });

                btnCardRemove.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {

                        alertDialog1.hide();
                        removeCard(position);


                    }
                });


            }
        }));
    }

    private void removeCard(int position) {
        Call<AddCard> removeCards = apiService.removeCard(cards.get(position).getId());
        removeCards.enqueue(new Callback<AddCard>() {
            @Override
            public void onResponse(Call<AddCard> call, Response<AddCard> response) {

                try {

                    CardsCall();


                } catch (Exception e) {
                    Log.d("onResponse", "There is an error");
                    e.printStackTrace();
                }


            }

            @Override
            public void onFailure(Call<AddCard> call, Throwable t) {

            }
        });

    }


    private void renameCard(final int position) {


        // get new_board_prompt.xml view
        LayoutInflater li = LayoutInflater.from(getContext());
        View promptsView = li.inflate(R.layout.new_board_prompt, null);

        AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(
                getContext());

        // set new_board_prompt.xml to alertdialog builder
        alertDialogBuilder.setView(promptsView);

        final EditText userInput = (EditText) promptsView
                .findViewById(R.id.prompt_edit_text);
        final TextView tv = (TextView) promptsView.findViewById(R.id.prompt_text_view);
        tv.setText("Rename Your Card: " + cards.get(position).getName() + "  to ");
        tv.setHint("Card Name");
        // set dialog message
        alertDialogBuilder
                .setCancelable(false)
                .setPositiveButton("OK",
                        new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog, int id) {
                                Call<AddCard> call = apiService.renameCard(cards.get(position).getId(), userInput.getText().toString());
                                call.enqueue(new Callback<AddCard>() {
                                    @Override
                                    public void onResponse(Call<AddCard> call, Response<AddCard> response) {
                                        try {

                                            CardsCall();
                                        } catch (Exception e) {
                                            Log.d("onResponse", "There is an error");
                                            e.printStackTrace();
                                        }
                                    }

                                    @Override
                                    public void onFailure(Call<AddCard> call, Throwable t) {

                                    }
                                });
                            }
                        })
                .setNegativeButton("Cancel",
                        new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog, int id) {
                                dialog.cancel();
                            }
                        });

        // create alert dialog
        AlertDialog alertDialog = alertDialogBuilder.create();

        // show it
        alertDialog.show();
    }
}
