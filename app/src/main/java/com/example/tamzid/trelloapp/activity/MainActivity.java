package com.example.tamzid.trelloapp.activity;

import android.app.ListActivity;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.example.tamzid.trelloapp.R;
import com.example.tamzid.trelloapp.adapter.BoardsAdapter;
import com.example.tamzid.trelloapp.model.Board;
import com.example.tamzid.trelloapp.model.NewBoard;
import com.example.tamzid.trelloapp.rest.ApiClient;
import com.example.tamzid.trelloapp.rest.ApiInterface;
import com.example.tamzid.trelloapp.utill.RecyclerTouchListener;

import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class MainActivity extends AppCompatActivity {
    private static final String TAG = MainActivity.class.getSimpleName();
    private RecyclerView recyclerView;
    private ProgressDialog pDialog;
    List<Board> boards;
    NewBoard newBoards;
    String idNewBoard, nameNewBoard;
    Intent intent;
    final Context context = this;
    ApiInterface apiService =
            ApiClient.getClient().create(ApiInterface.class);


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        pDialog = new ProgressDialog(this);
        pDialog.setMessage("Loading...");
        pDialog.show();
        pDialog.setCancelable(false);

        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        FloatingActionButton fab = (FloatingActionButton) findViewById(R.id.fab);
        fab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                FabCreateNewBoard(); //declier new board prompt layout dialog

            }
        });


        recyclerView = (RecyclerView) findViewById(R.id.Boards_recycler_view);
        recyclerView.setLayoutManager(new LinearLayoutManager(this));
        recyclerView.setItemAnimator(new DefaultItemAnimator());
        recyclerView.setHasFixedSize(true);
        ListBoard();

    }


    private void ListBoard() {

        Call<List<Board>> call = apiService.getBoard();
        call.enqueue(new Callback<List<Board>>() {
            @Override
            public void onResponse(Call<List<Board>> call, Response<List<Board>> response) {
                try {
                    boards = response.body();

                    pDialog.hide();

                    recyclerView.setAdapter(new BoardsAdapter(boards, R.layout.list_item_boards, getApplicationContext()));
                    recyclerViewItemClick();   //recylcler view item click ...


                } catch (Exception e) {
                    Log.d("onResponse", "There is an error");
                    e.printStackTrace();
                }

            }

            @Override
            public void onFailure(Call<List<Board>> call, Throwable t) {
                Log.e(TAG, t.toString());
            }
        });
    }

    //click on fab icon ..than show a prompt message and create a new board
    private void FabCreateNewBoard() {

        // get new_board_prompt.xml view
        LayoutInflater li = LayoutInflater.from(context);
        View promptsView = li.inflate(R.layout.new_board_prompt, null);

        AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(
                context);

        // set new_board_prompt.xml to alertdialog builder
        alertDialogBuilder.setView(promptsView);

        final EditText userInput = (EditText) promptsView
                .findViewById(R.id.prompt_edit_text);
        final TextView tv = (TextView) promptsView.findViewById(R.id.prompt_text_view);
        tv.setText("Create Your New Board: ");
        // set dialog message
        alertDialogBuilder
                .setCancelable(false)
                .setPositiveButton("OK",
                        new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog, int id) {

                                Call<NewBoard> call = apiService.postNewBoard(userInput.getText().toString());
                                call.enqueue(new Callback<NewBoard>() {
                                    @Override
                                    public void onResponse(Call<NewBoard> call, Response<NewBoard> response) {

                                        try {
                                            //newBoards = response.body();
                                            idNewBoard = response.body().getId();
                                            nameNewBoard = response.body().getName();

                                            intent = new Intent(MainActivity.this, com.example.tamzid.trelloapp.activity.ListActivity.class);
                                            intent.putExtra("name", nameNewBoard);
                                            intent.putExtra("id", idNewBoard);
                                            startActivity(intent);

                                        } catch (Exception e) {
                                            Log.d("onResponse", "There is an error");
                                            e.printStackTrace();
                                        }
                                    }

                                    @Override
                                    public void onFailure(Call<NewBoard> call, Throwable t) {

                                    }
                                });
                            }
                        })
                .setNegativeButton("Cancel",
                        new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog, int id) {
                                dialog.cancel();
                            }
                        });

        // create alert dialog
        AlertDialog alertDialog = alertDialogBuilder.create();

        // show it
        alertDialog.show();

    }

    //working with recycler view single item,,,
    private void recyclerViewItemClick() {
        recyclerView.addOnItemTouchListener(new RecyclerTouchListener(getApplicationContext(), recyclerView, new RecyclerTouchListener.ClickListener() {
            @Override
            public void onClick(View view, int position) {
                // Board board = boards.get(position);

                intent = new Intent(MainActivity.this, com.example.tamzid.trelloapp.activity.ListActivity.class);
                intent.putExtra("name", boards.get(position).getName());
                intent.putExtra("id", boards.get(position).getId());
                Log.i("boardid", boards.get(position).getId());
                startActivity(intent);


                // Toast.makeText(getApplicationContext(), boards.get(position).getId() + " is selected!", Toast.LENGTH_SHORT).show();
            }

            @Override
            public void onLongClick(View view, final int position) {

                LayoutInflater li = LayoutInflater.from(context);
                View promptsView = li.inflate(R.layout.board_longclick_prompt, null);

                AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(
                        context);
                final Button btnBoardLeave = (Button) promptsView.findViewById(R.id.button_board_leave);
                final Button btnBoardLinkShare = (Button) promptsView.findViewById(R.id.button_board_link_share);
                final Button btnRenameBoard = (Button) promptsView.findViewById(R.id.button_board_rename);

                // set new_board_prompt.xml to alertdialog builder
                alertDialogBuilder.setView(promptsView);
                alertDialogBuilder.setCancelable(true).setTitle(boards.get(position).getName());
                // create alert dialog
                final AlertDialog alertDialog = alertDialogBuilder.create();
                // show it
                alertDialog.show();
                btnRenameBoard.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {

                        alertDialog.hide();

                        updateBoardName(position);


                    }
                });


                btnBoardLeave.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {


                    }
                });

                btnBoardLinkShare.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {


                        Intent sendIntent = new Intent();
                        sendIntent.setAction(Intent.ACTION_SEND);
                        sendIntent.putExtra(Intent.EXTRA_TEXT, boards.get(position).getUrl());
                        sendIntent.setType("text/plain");

// Verify that the intent will resolve to an activity
                        if (sendIntent.resolveActivity(getPackageManager()) != null) {
                            startActivity(sendIntent);
                        }


                    }
                });

            }
        }));
    }

    private void updateBoardName(final int position) {


        // get new_board_prompt.xml view
        LayoutInflater li = LayoutInflater.from(context);
        View promptsView = li.inflate(R.layout.new_board_prompt, null);

        AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(
                context);

        // set new_board_prompt.xml to alertdialog builder
        alertDialogBuilder.setView(promptsView);

        final EditText userInput = (EditText) promptsView
                .findViewById(R.id.prompt_edit_text);
        final TextView tv = (TextView) promptsView.findViewById(R.id.prompt_text_view);
        tv.setText("Rename Your Board: " + boards.get(position).getName() + " to");
        tv.setHint("Board Name");
        // set dialog message
        alertDialogBuilder
                .setCancelable(false)
                .setPositiveButton("OK",
                        new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog, int id) {

                                Call<NewBoard> call = apiService.updateBoardName(boards.get(position).getId(), userInput.getText().toString());
                                call.enqueue(new Callback<NewBoard>() {
                                    @Override
                                    public void onResponse(Call<NewBoard> call, Response<NewBoard> response) {

                                        try {
                                            ListBoard();


                                        } catch (Exception e) {
                                            Log.d("onResponse", "There is an error");
                                            e.printStackTrace();
                                        }
                                    }

                                    @Override
                                    public void onFailure(Call<NewBoard> call, Throwable t) {

                                    }
                                });


                            }
                        })
                .setNegativeButton("Cancel",
                        new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog, int id) {
                                dialog.cancel();
                            }
                        });

        // create alert dialog
        AlertDialog alertDialog = alertDialogBuilder.create();

        // show it
        alertDialog.show();
    }


    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_search) {
            Toast.makeText(getApplicationContext(), "Search are selected", Toast.LENGTH_LONG).show();
        } else if (id == R.id.action_notification) {
            Toast.makeText(getApplicationContext(), "Notification are selected", Toast.LENGTH_LONG).show();
        } else if (id == R.id.action_my_cards) {
            Toast.makeText(getApplicationContext(), "My Cards are selected", Toast.LENGTH_LONG).show();
        } else if (id == R.id.action_help) {
            Toast.makeText(getApplicationContext(), "Help are selected", Toast.LENGTH_LONG).show();
        } else if (id == R.id.action_settings) {
            Toast.makeText(getApplicationContext(), "Settings are selected", Toast.LENGTH_LONG).show();
        }

        return super.onOptionsItemSelected(item);
    }

}
