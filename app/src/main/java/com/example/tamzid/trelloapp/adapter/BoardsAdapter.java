package com.example.tamzid.trelloapp.adapter;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.TextView;

import java.util.List;

import com.example.tamzid.trelloapp.R;
import com.example.tamzid.trelloapp.model.Board;

/**
 * Created by TAMZID on 12/11/2016.
 */

public class BoardsAdapter extends RecyclerView.Adapter<BoardsAdapter.BoardViewHolder> {

    private List<Board> boards;
    private int rowLayout;
    private Context context;


    public static class BoardViewHolder extends RecyclerView.ViewHolder {
        LinearLayout boardsLayout;
        TextView name;
        //TextView id;


        public BoardViewHolder(View v) {
            super(v);
            boardsLayout = (LinearLayout) v.findViewById(R.id.borads_layout);
            name = (TextView) v.findViewById(R.id.text_view_name);
            //  id = (TextView) v.findViewById(R.id.text_view_id);
        }
    }

    public BoardsAdapter(List<Board> boards, int rowLayout, Context context) {
        this.boards = boards;
        this.rowLayout = rowLayout;
        this.context = context;
    }

    @Override
    public BoardsAdapter.BoardViewHolder onCreateViewHolder(ViewGroup parent,
                                                            int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(rowLayout, parent, false);
        return new BoardsAdapter.BoardViewHolder(view);
    }


    @Override
    public void onBindViewHolder(BoardsAdapter.BoardViewHolder holder, final int position) {
        holder.name.setText(boards.get(position).getName());
        //    holder.id.setText(boards.get(position).getId());
    }

    @Override
    public int getItemCount() {
        return boards.size();
    }
}