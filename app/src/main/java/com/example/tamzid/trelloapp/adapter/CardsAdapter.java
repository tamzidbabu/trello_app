package com.example.tamzid.trelloapp.adapter;

import android.app.Application;
import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.example.tamzid.trelloapp.R;
import com.example.tamzid.trelloapp.model.Cards;

import java.util.List;

/**
 * Created by TAMZID on 12/11/2016.
 */

public class CardsAdapter extends RecyclerView.Adapter<CardsAdapter.CardsViewHolder> {

    private List<Cards> cards;
    private int rowLayout;
    private Context context;


    public static class CardsViewHolder extends RecyclerView.ViewHolder {
        LinearLayout boardsLayout;
        TextView name;
        TextView desc;

        //TextView id;
        public CardsViewHolder(View v) {
            super(v);
            boardsLayout = (LinearLayout) v.findViewById(R.id.cards_layout);
            name = (TextView) v.findViewById(R.id.text_view_cards);
            desc = (TextView) v.findViewById(R.id.desc_img_view);
            //  id = (TextView) v.findViewById(R.id.text_view_id);
        }
    }

    public CardsAdapter(List<Cards> cards, int rowLayout, Context context) {
        this.cards = cards;
        this.rowLayout = rowLayout;
        this.context = context;
    }

    @Override
    public CardsAdapter.CardsViewHolder onCreateViewHolder(ViewGroup parent,
                                                           int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(rowLayout, parent, false);
        return new CardsAdapter.CardsViewHolder(view);
    }


    @Override
    public void onBindViewHolder(CardsAdapter.CardsViewHolder holder, final int position) {
        holder.name.setText(cards.get(position).getName());
        holder.desc.setText(cards.get(position).getDesc());
        //cards.clear();
        //    holder.id.setText(boards.get(position).getId());

    }

    @Override
    public int getItemCount() {

        return cards.size();

    }

//    List<Cards> mcards;
//    public void swap(List<Cards> mcards){
//        if (cards != null) {
//            cards.clear();
//            cards.addAll(mcards);
//        }
//        else {
//            cards = mcards;
//        }
//        notifyDataSetChanged();
//    }


}