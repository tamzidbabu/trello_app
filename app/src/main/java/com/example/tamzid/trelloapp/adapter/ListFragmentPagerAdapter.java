package com.example.tamzid.trelloapp.adapter;

import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;
import android.util.Log;

import com.example.tamzid.trelloapp.activity.AddListFragment;
import com.example.tamzid.trelloapp.model.Cards;
import com.example.tamzid.trelloapp.model.Lists;
import com.example.tamzid.trelloapp.activity.ListFragments;

import java.util.List;

/**
 * Created by TAMZID on 12/27/2016.
 */

public class ListFragmentPagerAdapter extends FragmentPagerAdapter {

    private static final String TAG = "MyFragmentPagerAdapter";
    private List<Lists> mListsItem;
    private FragmentManager mFragmentManager;

    public ListFragmentPagerAdapter(FragmentManager fm, List<Lists> listsItem) {
        super(fm);
        this.mFragmentManager = fm;
        this.mListsItem = listsItem;
    }

    @Override
    public int getCount() {
        return mListsItem.size() + 1;
    }


    @Override
    public int getItemPosition(Object object) {
        List<Fragment> fragmentsList = mFragmentManager.getFragments();
        ListFragments fragment = (ListFragments) object;
        Lists listsItem = fragment.getListsItem();

        int position = mListsItem.indexOf(listsItem);

        if (position >= 0) {
            // The current data matches the data in this active fragment, so let it be as it is.
            return position;
        } else {
            // Returning POSITION_NONE means the current data does not matches the data this fragment is showing right now.  Returning POSITION_NONE constant will force the fragment to redraw its view layout all over again and show new data.
            return POSITION_NONE;
        }
    }

    @Override
    public Fragment getItem(int position) {
        //We are doing this only for checking the total number of fragments in the fragment manager.
        List<Fragment> fragmentsList = mFragmentManager.getFragments();
        int size = 0;
        if (fragmentsList != null) {
            size = fragmentsList.size();
        }
        AddListFragment fragment1 = new AddListFragment();
        int listPosition = 0;
        if (mListsItem.size() > position) {
            Lists listItem = mListsItem.get(position);

            //   Log.i(TAG, "********getItem position:" + position + " size:" + size + " name:" + listItem.getName() + " Id:" + listItem.getId());

            //Create a new instance of the fragment and return it.
            ListFragments listFragments = ListFragments.getInstance(/*dummyItem.getImageUrl(), dummyItem.getImageTitle()*/);
            //We will not pass the data through bundle because it will not gets updated by calling notifyDataSetChanged()  method. We will do it through getter and setter.
            listFragments.setListsItem(listItem);

            return listFragments;
        } else {
            return fragment1;
        }
    }


}
