
package com.example.tamzid.trelloapp.model;

import java.util.List;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class Board {

    @SerializedName("name")
    @Expose
    private String name;
    @SerializedName("desc")
    @Expose
    private String desc;
    @SerializedName("descData")
    @Expose
    private Object descData;
    @SerializedName("closed")
    @Expose
    private boolean closed;
    @SerializedName("idOrganization")
    @Expose
    private Object idOrganization;
    @SerializedName("pinned")
    @Expose
    private Object pinned;
    @SerializedName("invitations")
    @Expose
    private Object invitations;
    @SerializedName("shortLink")
    @Expose
    private String shortLink;
    @SerializedName("powerUps")
    @Expose
    private List<Object> powerUps = null;
    @SerializedName("dateLastActivity")
    @Expose
    private Object dateLastActivity;
    @SerializedName("idTags")
    @Expose
    private List<Object> idTags = null;
    @SerializedName("id")
    @Expose
    private String id;
    @SerializedName("invited")
    @Expose
    private boolean invited;
    @SerializedName("starred")
    @Expose
    private boolean starred;
    @SerializedName("url")
    @Expose
    private String url;

    @SerializedName("shortUrl")
    @Expose
    private String shortUrl;

    /**
     * @return The name
     */
    public String getName() {
        return name;
    }

    /**
     * @param name The name
     */
    public void setName(String name) {
        this.name = name;
    }

    /**
     * @return The desc
     */
    public String getDesc() {
        return desc;
    }

    /**
     * @param desc The desc
     */
    public void setDesc(String desc) {
        this.desc = desc;
    }

    /**
     * @return The descData
     */
    public Object getDescData() {
        return descData;
    }

    /**
     * @param descData The descData
     */
    public void setDescData(Object descData) {
        this.descData = descData;
    }

    /**
     * @return The closed
     */
    public boolean isClosed() {
        return closed;
    }

    /**
     * @param closed The closed
     */
    public void setClosed(boolean closed) {
        this.closed = closed;
    }

    /**
     * @return The idOrganization
     */
    public Object getIdOrganization() {
        return idOrganization;
    }

    /**
     * @param idOrganization The idOrganization
     */
    public void setIdOrganization(Object idOrganization) {
        this.idOrganization = idOrganization;
    }

    /**
     * @return The pinned
     */
    public Object getPinned() {
        return pinned;
    }

    /**
     * @param pinned The pinned
     */
    public void setPinned(Object pinned) {
        this.pinned = pinned;
    }

    /**
     * @return The invitations
     */
    public Object getInvitations() {
        return invitations;
    }

    /**
     * @param invitations The invitations
     */
    public void setInvitations(Object invitations) {
        this.invitations = invitations;
    }

    /**
     * @return The shortLink
     */
    public String getShortLink() {
        return shortLink;
    }

    /**
     * @param shortLink The shortLink
     */
    public void setShortLink(String shortLink) {
        this.shortLink = shortLink;
    }

    /**
     * @return The powerUps
     */
    public List<Object> getPowerUps() {
        return powerUps;
    }

    /**
     * @param powerUps The powerUps
     */
    public void setPowerUps(List<Object> powerUps) {
        this.powerUps = powerUps;
    }

    /**
     * @return The dateLastActivity
     */
    public Object getDateLastActivity() {
        return dateLastActivity;
    }

    /**
     * @param dateLastActivity The dateLastActivity
     */
    public void setDateLastActivity(Object dateLastActivity) {
        this.dateLastActivity = dateLastActivity;
    }

    /**
     * @return The idTags
     */
    public List<Object> getIdTags() {
        return idTags;
    }

    /**
     * @param idTags The idTags
     */
    public void setIdTags(List<Object> idTags) {
        this.idTags = idTags;
    }

    /**
     * @return The id
     */
    public String getId() {
        return id;
    }

    /**
     * @param id The id
     */
    public void setId(String id) {
        this.id = id;
    }

    /**
     * @return The invited
     */
    public boolean isInvited() {
        return invited;
    }

    /**
     * @param invited The invited
     */
    public void setInvited(boolean invited) {
        this.invited = invited;
    }

    /**
     * @return The starred
     */
    public boolean isStarred() {
        return starred;
    }

    /**
     * @param starred The starred
     */
    public void setStarred(boolean starred) {
        this.starred = starred;
    }

    /**
     * @return The url
     */
    public String getUrl() {
        return url;
    }

    /**
     * @param url The url
     */
    public void setUrl(String url) {
        this.url = url;
    }

    /**
     * @return The shortUrl
     */
    public String getShortUrl() {
        return shortUrl;
    }

    /**
     * @param shortUrl The shortUrl
     */
    public void setShortUrl(String shortUrl) {
        this.shortUrl = shortUrl;
    }

}
