package com.example.tamzid.trelloapp.model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

/**
 * Created by TAMZID on 12/14/2016.
 */

public class Cards {


    @SerializedName("id")
    @Expose
    private String id;
    @SerializedName("checkItemStates")
    @Expose
    private Object checkItemStates;
    @SerializedName("closed")
    @Expose
    private boolean closed;
    @SerializedName("dateLastActivity")
    @Expose
    private String dateLastActivity;
    @SerializedName("desc")
    @Expose
    private String desc;
    @SerializedName("descData")
    @Expose
    private Object descData;
    @SerializedName("idBoard")
    @Expose
    private String idBoard;
    @SerializedName("idList")
    @Expose
    private String idList;

    @SerializedName("idShort")
    @Expose
    private int idShort;
    @SerializedName("idAttachmentCover")
    @Expose
    private Object idAttachmentCover;
    @SerializedName("manualCoverAttachment")
    @Expose
    private boolean manualCoverAttachment;

    @SerializedName("name")
    @Expose
    private String name;
    @SerializedName("pos")
    @Expose
    private int pos;
    @SerializedName("shortLink")
    @Expose
    private String shortLink;

    @SerializedName("dueComplete")
    @Expose
    private boolean dueComplete;
    @SerializedName("due")
    @Expose
    private Object due;

    @SerializedName("shortUrl")
    @Expose
    private String shortUrl;
    @SerializedName("subscribed")
    @Expose
    private boolean subscribed;
    @SerializedName("url")
    @Expose
    private String url;

    /**
     * @return The id
     */
    public String getId() {
        return id;
    }

    /**
     * @param id The id
     */
    public void setId(String id) {
        this.id = id;
    }

    /**
     * @return The checkItemStates
     */
    public Object getCheckItemStates() {
        return checkItemStates;
    }

    /**
     * @param checkItemStates The checkItemStates
     */
    public void setCheckItemStates(Object checkItemStates) {
        this.checkItemStates = checkItemStates;
    }

    /**
     * @return The closed
     */
    public boolean isClosed() {
        return closed;
    }

    /**
     * @param closed The closed
     */
    public void setClosed(boolean closed) {
        this.closed = closed;
    }

    /**
     * @return The dateLastActivity
     */
    public String getDateLastActivity() {
        return dateLastActivity;
    }

    /**
     * @param dateLastActivity The dateLastActivity
     */
    public void setDateLastActivity(String dateLastActivity) {
        this.dateLastActivity = dateLastActivity;
    }

    /**
     * @return The desc
     */
    public String getDesc() {
        return desc;
    }

    /**
     * @param desc The desc
     */
    public void setDesc(String desc) {
        this.desc = desc;
    }

    /**
     * @return The descData
     */
    public Object getDescData() {
        return descData;
    }

    /**
     * @param descData The descData
     */
    public void setDescData(Object descData) {
        this.descData = descData;
    }

    /**
     * @return The idBoard
     */
    public String getIdBoard() {
        return idBoard;
    }

    /**
     * @param idBoard The idBoard
     */
    public void setIdBoard(String idBoard) {
        this.idBoard = idBoard;
    }

    /**
     * @return The idList
     */
    public String getIdList() {
        return idList;
    }

    /**
     * @param idList The idList
     */
    public void setIdList(String idList) {
        this.idList = idList;
    }

    /**
     *
     * @return
     *     The idMembersVoted

    /**
     *
     * @param idMembersVoted
     *     The idMembersVoted
     */


    /**
     * @return The idShort
     */
    public int getIdShort() {
        return idShort;
    }

    /**
     * @param idShort The idShort
     */
    public void setIdShort(int idShort) {
        this.idShort = idShort;
    }

    /**
     * @return The idAttachmentCover
     */
    public Object getIdAttachmentCover() {
        return idAttachmentCover;
    }

    /**
     * @param idAttachmentCover The idAttachmentCover
     */
    public void setIdAttachmentCover(Object idAttachmentCover) {
        this.idAttachmentCover = idAttachmentCover;
    }

    /**
     * @return The manualCoverAttachment
     */
    public boolean isManualCoverAttachment() {
        return manualCoverAttachment;
    }

    /**
     * @param manualCoverAttachment The manualCoverAttachment
     */
    public void setManualCoverAttachment(boolean manualCoverAttachment) {
        this.manualCoverAttachment = manualCoverAttachment;
    }

    /**
     *
     * @return
     *     The idLabels
     */


    /**
     * @return The name
     */
    public String getName() {
        return name;
    }

    /**
     * @param name The name
     */
    public void setName(String name) {
        this.name = name;
    }

    /**
     * @return The pos
     */
    public int getPos() {
        return pos;
    }

    /**
     * @param pos The pos
     */
    public void setPos(int pos) {
        this.pos = pos;
    }

    /**
     * @return The shortLink
     */
    public String getShortLink() {
        return shortLink;
    }

    /**
     * @param shortLink The shortLink
     */
    public void setShortLink(String shortLink) {
        this.shortLink = shortLink;
    }

    /**
     *
     * @return
     *     The badges
     */


    /**
     * @return The dueComplete
     */
    public boolean isDueComplete() {
        return dueComplete;
    }

    /**
     * @param dueComplete The dueComplete
     */
    public void setDueComplete(boolean dueComplete) {
        this.dueComplete = dueComplete;
    }

    /**
     * @return The due
     */
    public Object getDue() {
        return due;
    }

    /**
     * @param due The due
     */
    public void setDue(Object due) {
        this.due = due;
    }

    /**
     *
     * @return
     *     The idChecklists
     */


    /**
     *
     * @param idMembers
     *     The idMembers
     */


    /**
     * @return The shortUrl
     */
    public String getShortUrl() {
        return shortUrl;
    }

    /**
     * @param shortUrl The shortUrl
     */
    public void setShortUrl(String shortUrl) {
        this.shortUrl = shortUrl;
    }

    /**
     * @return The subscribed
     */
    public boolean isSubscribed() {
        return subscribed;
    }

    /**
     * @param subscribed The subscribed
     */
    public void setSubscribed(boolean subscribed) {
        this.subscribed = subscribed;
    }

    /**
     * @return The url
     */
    public String getUrl() {
        return url;
    }

    /**
     * @param url The url
     */
    public void setUrl(String url) {
        this.url = url;
    }


}
