
package com.example.tamzid.trelloapp.model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class Lists {

    @SerializedName("id")
    @Expose
    private String id;
    @SerializedName("name")
    @Expose
    private String name;
    @SerializedName("closed")
    @Expose
    private boolean closed;
    @SerializedName("idBoard")
    @Expose
    private String idBoard;
    @SerializedName("pos")
    @Expose
    private double pos;
    @SerializedName("subscribed")
    @Expose
    private boolean subscribed;

    public Lists(String id, String name) {
        this.id = id;
        this.name = name;
    }

    /**
     * 
     * @return
     *     The id
     */

    public String getId() {
        return id;
    }

    /**
     * 
     * @param id
     *     The id
     */
    public void setId(String id) {
        this.id = id;
    }

    /**
     * 
     * @return
     *     The name
     */
    public String getName() {
        return name;
    }

    /**
     * 
     * @param name
     *     The name
     */
    public void setName(String name) {
        this.name = name;
    }

    /**
     * 
     * @return
     *     The closed
     */
    public boolean isClosed() {
        return closed;
    }

    /**
     * 
     * @param closed
     *     The closed
     */
    public void setClosed(boolean closed) {
        this.closed = closed;
    }

    /**
     * 
     * @return
     *     The idBoard
     */
    public String getIdBoard() {
        return idBoard;
    }

    /**
     * 
     * @param idBoard
     *     The idBoard
     */
    public void setIdBoard(String idBoard) {
        this.idBoard = idBoard;
    }

    /**
     * 
     * @return
     *     The pos
     */
    public double getPos() {
        return pos;
    }

    /**
     * 
     * @param pos
     *     The pos
     */
    public void setPos(double pos) {
        this.pos = pos;
    }

    /**
     * 
     * @return
     *     The subscribed
     */
    public boolean isSubscribed() {
        return subscribed;
    }

    /**
     * 
     * @param subscribed
     *     The subscribed
     */
    public void setSubscribed(boolean subscribed) {
        this.subscribed = subscribed;
    }

}
