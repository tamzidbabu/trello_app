package com.example.tamzid.trelloapp.rest;

import com.example.tamzid.trelloapp.model.AddCard;
import com.example.tamzid.trelloapp.model.Board;
import com.example.tamzid.trelloapp.model.Cards;
import com.example.tamzid.trelloapp.model.Lists;
import com.example.tamzid.trelloapp.model.NewBoard;

import java.util.List;

import retrofit2.Call;
import retrofit2.http.DELETE;
import retrofit2.http.Field;
import retrofit2.http.FormUrlEncoded;
import retrofit2.http.GET;
import retrofit2.http.POST;
import retrofit2.http.PUT;
import retrofit2.http.Path;
import retrofit2.http.Query;


public interface ApiInterface {
    String API_AND_TOKEN_KEY = "key=dd58cf53da9b706d90149081d036250c&token=b68030b8b5f3db09a4bab78cd3d613b5c7917d4db580117123171416268c6bc0";


    //get boards data
    @GET("1/members/me/boards?" + API_AND_TOKEN_KEY)
    Call<List<Board>> getBoard();


    //get lists data
    @GET("1/board/{id}/lists?" + API_AND_TOKEN_KEY)
    Call<List<Lists>> getList(@Path("id") String listId);


    //add new board
    @FormUrlEncoded
    @POST("1/board?" + API_AND_TOKEN_KEY)
    Call<NewBoard> postNewBoard(@Field("name") String name);


    //add new card
    @FormUrlEncoded
    @POST("1/cards?" + API_AND_TOKEN_KEY)
    Call<AddCard> postNewCard(@Field("name") String name, @Field("desc") String desc, @Field("idList") String listId);


    //get cards data
    @GET("1/lists/{id}/cards?" + API_AND_TOKEN_KEY)
    Call<List<Cards>> getCards(@Path("id") String cardId);


    //rename board
    @FormUrlEncoded
    @PUT("1/boards/{id}/name?" + API_AND_TOKEN_KEY)
    Call<NewBoard> updateBoardName(@Path("id") String idBoard, @Field("value") String name);


    //rename card
    @FormUrlEncoded
    @PUT("1/cards/{id}/name?" + API_AND_TOKEN_KEY)
    Call<AddCard> renameCard(@Path("id") String idCard, @Field("value") String name);


    //remove cards
    @DELETE("1/cards/{id}?" + API_AND_TOKEN_KEY)
    Call<AddCard> removeCard(@Path("id") String id);

    //add list
    @FormUrlEncoded
    @POST("1/lists?" + API_AND_TOKEN_KEY)
    Call<NewBoard> addList(@Field("name") String name, @Field("idBoard") String idBoard);


}
